/*
This example creates an SSLclient object that connects and transfers
data using always SSL.

I then use it to connect to smpt.gmail.com and send an email.

SMTP command reference:  https://www.samlogic.net/articles/smtp-commands-reference.htm

Things to note:
* gmail sending account: must have 2 factor identification enabled and an app specific password created
*                        for this usage
* lots of things are tricky here!
*/

#include <SPI.h>
#include <WiFiNINA.h>
#include "helpers.h"
#include "arduino_secrets.h"


///////please enter your sensitive data in the Secret tab/arduino_secrets.h
const char ssid[] = SECRET_SSID;        // your network SSID (name)
const char pass[] = SECRET_PASS;        // your network password (use for WPA)

const String gAcc   = SECRET_SEND_ACCOUNT,           // "something@somewhere.com"
             gPass  = SECRET_SEND_ACCOUNT_PASSWORD;  // "the_password"

const int port = 465; //587 for TLS does not work
const char server[] = "smtp.gmail.com";    // name address for Gmail SMTP (using DNS)

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(115200);
  while (!Serial);
  
  int encodedLength = Base64.encodedLength(gAcc.length());
  char encodedAccount[encodedLength+1];
  encode64(gAcc, encodedAccount);
  encodedAccount[encodedLength] = '\0';
  
  encodedLength = Base64.encodedLength(gPass.length());
  char encodedPass[encodedLength+1];
  encode64(gPass, encodedPass);
  encodedPass[encodedLength] = '\0';
 
  Serial.println("Enter a character to start...");
  while (!Serial.available());
  Serial.read();

  //////////////////////  First Connect to WIFI /////////////////////////
  _connectToWifi(ssid,pass);

  ///////////////////// then connect to server ///////////////////////////

  Serial.println("\nConnecting to server: " + String(server) +":" +String(port));

  WiFiSSLClient client;
  
  if (client.connectSSL(server, port)==1){ 
    Serial.println("Connected to server");
    if (response(client) ==-1){
      String s = server + String(" port:")+ String(port);
      Serial.print("no reply on connect to ");
      Serial.println(s);
    }

    /////////////////////// now do the SMTP dance ////////////////////////////
    
    Serial.println("Sending Extended Hello: <start>EHLO yourDomain.org<end>");

    //// PLEASE UPDATE /////
    client.println("EHLO yourDomain.org");
    if (response(client) ==-1){
      Serial.println("no reply EHLO yourDomain.org");
    }
    
    Serial.println("Sending auth login: <start>AUTH LOGIN<end>");
    client.println(F("AUTH LOGIN"));
    if (response(client) ==-1){
      Serial.println("no reply AUTH LOGIN");
    }

    Serial.println("Sending account: <start>" +String(encodedAccount) + "<end>");
    client.println(F(encodedAccount)); 
    if (response(client) ==-1){
      Serial.println("no reply to Sending User");
    }
    
    Serial.println("Sending Password: <start>" +String(encodedPass) + "<end>");  
    client.println(F(encodedPass)); 
    if (response(client) ==-1){
      Serial.println("no reply Sending Password");
    }
    
    //// PLEASE UPDATE /////
    Serial.println("Sending From: <start>MAIL FROM: <somebody@gmail.com><end>");
    // your email address (sender) - MUST include angle brackets
    client.println(F("MAIL FROM: <somebody@gmail.com>"));
    if (response(client) ==-1){
      Serial.println("no reply Sending From");
    }

    //// PLEASE UPDATE /////
    // change to recipient address - MUST include angle brackets
    Serial.println("Sending To: <start>RCPT To: <somebodyelse@gmail.com><end>");
    client.println(F("RCPT To: <somebodyelse@gmail.com>"));
    if (response(client) ==-1){
      Serial.println("no reply Sending To");
    }
    
    Serial.println("Sending DATA: <start>DATA<end>");
    client.println(F("DATA"));
    if (response(client) ==-1){
      Serial.println("no reply Sending DATA");
    }

    //// PLEASE UPDATE /////
    Serial.println("Sending email: <start>");
    // recipient address (include option display name if you want)
    client.println(F("To: SomebodyElse <somebodyelse@gmail.com>"));

    //// PLEASE UPDATE /////
    const char outgoing[] = "Many things were fixed in this code, but many can still go wrong.\n";

    //// PLEASE UPDATE /////
    // send from address, subject and message
    client.println(F("From: SomeBody <somebody@gmail.com>"));
    client.println(F("Subject: Good 2 Go!"));
    client.println(F(outgoing));
  
    // IMPORTANT you must send a complete line containing just a "." to end the conversation
    // So the PREVIOUS line to this one must be a println not just a print
    client.println(F("."));
    if (response(client) ==-1){
      Serial.println("no reply Sending '.'");
    }
    
    Serial.println(F("Sending QUIT"));
    client.println(F("QUIT"));
    if (response(client) ==-1){
      Serial.println("no reply Sending QUIT");
    }
    client.stop();
  }
  else{
    Serial.println("failed to connect to server");
  }
  Serial.println("Done.");
}

void loop() {}
